#include "OutStream.h"
#include <stdio.h>

OutStream::OutStream()
{
	_file = stdout;
}

OutStream::~OutStream()
{
}

OutStream& OutStream::operator<<(const char *str)
{
	fprintf(_file,"%s", str);
	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	fprintf(_file,"%d", num);
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)(FILE* _file))
{
	pf(_file);
	return *this;
}


void endline(FILE* _file)//Had to change the signiture of this function because it doesn't belong to the class and I can't use "this->" so I had to have the stdout as a param.
{
	fprintf(_file,"\n");
}
