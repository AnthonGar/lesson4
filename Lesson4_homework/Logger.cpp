#include "Logger.h"

Logger::Logger()
{
	_startLine = true;
}

Logger::~Logger()
{
}

/*
This func will determine if we need to start a new log or not
*/
void Logger::setStartLine()
{
	static unsigned int counter = 0;//asked in	Q4->5->1
	if (_startLine)//If there is a new line we print a new log.
	{
		_os << "LOG #" << counter++ << ": ";
		_startLine = false;
	}
}

Logger& operator<<(Logger& l, const char *msg)
{
	l.setStartLine();//new line started?
	l._os << msg;
	return l;
}

Logger& operator<<(Logger& l, int num)
{
	l.setStartLine();//new line started?
	l._os << num;
	return l;
}

Logger& operator<<(Logger& l, void(*pf)(FILE*))
{
	l._startLine = true;//start new line -> new log
	l._os << pf;
	return l;
}
