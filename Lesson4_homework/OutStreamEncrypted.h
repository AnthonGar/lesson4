#pragma once
#include "OutStream.h"

class OutStreamEncrypted : public OutStream
{
private:
	unsigned int _offset;

public:
	OutStreamEncrypted(unsigned int offset);
	~OutStreamEncrypted();

	OutStreamEncrypted& operator<<(const char *str);
	OutStreamEncrypted& operator<<(int num);
	OutStreamEncrypted& operator<<(void(*pf)(FILE *out));

	void encrypt(char *str);
};
