#pragma warning(disable:4996)

#include "OutStreamEncrypted.h"
#include <string>

#define MAX_CHAR 126
#define MIN_CHAR 32

#define MAX_LEN 512

//Dtor.
OutStreamEncrypted::~OutStreamEncrypted()
{
}

//Ctor.
//input: offset to encrypt.
OutStreamEncrypted::OutStreamEncrypted(unsigned int offset)
{
	this->_offset = offset;
}

/*
This function will encrypt a string.
input: string.
output: none.
*/
void OutStreamEncrypted::encrypt(char *str)
{
	for (unsigned int i = 0; i < strlen(str); i++)
	{
		if (str[i] >= MIN_CHAR && str[i] <= MAX_CHAR)
		//If offset + currentChar are less greater then 126 so we need to loop back. if not, we just add them.
			str[i] = (this->_offset + str[i] > MAX_CHAR) ? ((this->_offset + str[i]) - MAX_CHAR - 1) + MIN_CHAR : this->_offset + str[i];
	}
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(const char *str)
{
	char encrypted[MAX_LEN];
	strcpy(encrypted,str);
	encrypt(encrypted);
	fprintf(_file, "%s", encrypted);
	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(int num)
{
	char encrypted[MAX_LEN];
	itoa(num, encrypted, 10);
	encrypt(encrypted);
	printf("%c", encrypted[1]);
	fprintf(_file, "%s", encrypted);
	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(void(*pf)(FILE *out))
{
	pf(_file);
	return *this;
}

