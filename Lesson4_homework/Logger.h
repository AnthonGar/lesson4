#pragma once
#include "OutStream.h"
#include <stdio.h>

class Logger
{
	OutStream _os;
	bool _startLine;
	void setStartLine();
public:
	Logger();
	~Logger();
	friend Logger& operator<<(Logger& l, const char *msg);
	friend Logger& operator<<(Logger& l, int num);
	friend Logger& operator<<(Logger& l, void(*pf)(FILE*));
};
