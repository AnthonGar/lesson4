#include "OutStream.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"

int main()
{
	const char* filePathExample = "C:\\Users\\anthon_pc\\Desktop\\LocalRep\\Lesson4_homework\\Files\\FileWrite.txt";

	//Part one:
	OutStream OutStreamExample;
	printf("Part one:\n");
	OutStreamExample << "I am the Doctor and I�m " << 1500 << " years old" << endline;

	//Part two:
	FileStream FileStreamExample(filePathExample);
	printf("Part two:\n");
	FileStreamExample << "I am the Doctor and I�m " << 1500 << " years old" << endline;

	//Part three:
	printf("Part three:\n");
	OutStreamEncrypted OutStreamEncryptedExample(2);
	OutStreamEncryptedExample << "I am the Doctor and I�m " << 1500 << " years old" << endline;

	//Part four:
	Logger loggerExample;
	loggerExample << "I am the doctor and I'm " << endline;
	loggerExample << 1500 << endline;
	loggerExample << " years old" << endline;

	getchar();
	return 0;
}
