#pragma once
#include "OutStream.h"

namespace mls {
	class FileStream : public OutStream
	{
	public:
		FileStream(const char *filename);
		~FileStream();
	};
}
