#include "FileStream.h"
#include <stdio.h>

using namespace mls;

/*
Ctor of FileStream class.
input: path to a file.
*/
FileStream::FileStream(const char *filename)
{
	errno_t open = fopen_s(&_file, filename, "w");
	if (open != 0)//If fopne_s fails.
	{
		printf("Can't Open File!\n");
	}
}

/*
Dtor of FileStream class, it doesn't have dynamic allocation so I just have to close the opened file.
*/
FileStream::~FileStream()
{
	fclose(_file);
}
