#pragma once
#include <stdio.h>

namespace mls {
	class OutStream
	{
	protected:
		FILE * _file;

	public:
		OutStream();
		~OutStream();

		OutStream& operator<<(const char *str);
		OutStream& operator<<(int num);
		OutStream& operator<<(void(*pf)(FILE* _file));
	};

	void endline(FILE* _file);
}
