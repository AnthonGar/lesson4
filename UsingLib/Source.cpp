#include "OutStream.h"
#include "FileStream.h"

int main()
{
	const char* filePathExample = "C:\\Users\\anthon_pc\\Desktop\\LocalRep\\Lesson4_homework\\Files\\FileWrite.txt";

	//Part one:
	OutStream OutStreamExample;
	printf("Part one:\n");
	OutStreamExample << "I am the Doctor and I�m " << 1500 << " years old" << endline;

	//Part two:
	FileStream FileStreamExample(filePathExample);
	printf("Part two:\n");
	FileStreamExample << "I am the Doctor and I�m " << 1500 << " years old" << endline;

	return 0;
}
